<?php
/**
 * Plugin Name:     Launchpad
 * Plugin URI:      https://www.addpeople.co.uk
 * Description:     Connects your website to the Launchpad website control system
 * Author:          Jessica Plant @ Add People
 * Author URI:      https://www.addpeople.co.uk
 * Text Domain:     launchpad
 * Domain Path:     /languages
 * Version:         0.1.1
 *
 * @package         Launchpad
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function heartbeat() {
	return array(
		'state_meta'   => array(
			'timestamp' => time()
		),
		'content_meta' => array(
			'count_posts' => wp_count_posts(),
			'count_pages' => wp_count_posts( 'page' ),
		),
		'user_meta'    => array(
			'count_users' => count_users()
		)
	);
}

function check_wpx_is_enabled() {
	$theme = wp_get_theme();

	$colour_1 = get_field('primary_colour_picker', 'options');
	$colour_2 = get_field('secondary_colour_picker', 'options');
	$colour_3 = get_field('cta_colour_picker', 'options');

	return array(
		'name'    => $theme->get( 'Name' ),
		'version' => $theme->get( 'Version' ),
		'colours' => array(
			'colour_1' => $colour_1,
			'colour_2' => $colour_2,
			'colour_3' => $colour_3,
		)
	);
}

function fetch_wpx_pages() { //TODO sanitise and secure this response
	$pages = get_posts(array(
		'post_type' => 'page'
	));
	return $pages;
}

function fetch_wpx_page($request) {
	$content_post = get_post($request['id']);
	$title = $content_post->post_title;
	$content = $content_post->post_content;
	$content = trim(preg_replace('/\s\s+/', ',', $content));
	$content = explode(',', $content);
	// $content = str_replace(']]>', ']]&gt;', $content);
	return array(
		'meta' => array(
			'id' => $request['id'],
			'title' => $title
		),
		'layout' => $content
	);
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'launchpad/v1', '/heartbeat', array(
		'methods'  => 'GET',
		'callback' => 'heartbeat',
	) );
	register_rest_route( 'launchpad/v1', '/wpx', array(
		'methods'  => 'GET',
		'callback' => 'check_wpx_is_enabled',
	) );
	register_rest_route( 'launchpad/v1', '/wpx/pages', array(
		'methods'  => 'GET',
		'callback' => 'fetch_wpx_pages',
	) );
	register_rest_route( 'launchpad/v1', '/wpx/page/(?P<id>\d+)', array(
		'methods'  => 'GET',
		'callback' => 'fetch_wpx_page',
	) );
} );